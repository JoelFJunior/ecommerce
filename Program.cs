var builder = WebApplication.CreateBuilder(args);

// add middlewares
builder.Services.AddControllers();

var app = builder.Build();

// setup middleware
app.MapControllerRoute("default", "/{controller=Cliente}/{action=Index}");
// app.MapControllers(); // using [Route("")]

app.Run();


